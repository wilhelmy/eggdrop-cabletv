if {[info commands urlmagic::plugins::sqlite::db] == ""} {
	putlog "urlmagic sqlite plugin must be loaded"
	return 1
}

proc random_gif {chan} {
	urlmagic::plugins::sqlite::db eval {
		SELECT url FROM urls
		WHERE content_type = 'image/gif' COLLATE NOCASE
		ORDER BY random()
		LIMIT 1
	} {
		puthelp "PRIVMSG [chandname2name $chan] :$url"
	}
}

proc pub:gif {nick uhost hand chan text} {
	random_gif $chan
	return
}

bind pub -|- !gif pub:gif
bind pub -|- !gifbot pub:gif
bind pub -|- !pizpot pub:gif
