#!/usr/bin/env perl
# http://cdn.meme.am/instances/500x/9717485.jpg
#
# Produce high-quality entertainment URLs on stdout when poked by \n on stdin

use strict;
use warnings;
use DBI;
use LWP::UserAgent;

sub Log {
	print STDERR @_, "\n";
}

Log "--- entertained restarted on ".localtime;

my $dbfile = shift @ARGV;
my $dbh = DBI->connect("dbi:SQLite:dbname=$dbfile","","");

our $ua = LWP::UserAgent->new;
$ua->agent('GifBot/0.1');

$dbh->prepare(<<SQL)->execute or die "$!";

CREATE VIEW IF NOT EXISTS moving_pictures AS
  SELECT id, url FROM urls
  WHERE dead_link = 0 AND (
        content_type = 'image/gif'
     OR content_type = 'video/webm'
     OR content_type = 'video/mp4'
     OR url LIKE 'http%://i.imgur.com/%.gifv'
  ) COLLATE NOCASE

SQL

$dbh->do(<<SQL) or die $!;

CREATE VIEW IF NOT EXISTS entertainment AS
  SELECT id, url FROM urls
  WHERE dead_link = 0 AND (
        content_type = 'image/gif'
     OR content_type = 'image/jpeg'
     OR content_type = 'image/png'
     OR content_type = 'video/webm'
     OR content_type = 'video/mp4'
     OR url LIKE 'http%://i.imgur.com/%.gifv'
     OR url LIKE 'http%://i.imgur.com/%'
     OR url LIKE 'http%://imgur.com/gallery/%'
  ) COLLATE NOCASE

SQL

our $gif = $dbh->prepare(<<SQL);

SELECT id, url FROM moving_pictures
  ORDER BY RANDOM()
  LIMIT 1

SQL

our $entertain = $dbh->prepare(<<SQL);

SELECT id, url FROM entertainment
  ORDER BY RANDOM()
  LIMIT 1

SQL

our $kill = $dbh->prepare(<<SQL);

UPDATE urls SET dead_link = 1 WHERE id = ?

SQL

sub nuke_url {
	my $id = shift;

	$kill->execute($id);
	$kill->finish;

	0
}

sub grab_url {
	my $sth = shift eq 'any' ? $entertain : $gif;
	$sth->execute;
	my @row = $sth->fetchrow_array;
	$sth->finish;
	return @row;
}

sub check_url {
	my ($id, $url, $seen) = @_;
	my $req = HTTP::Request->new(HEAD => $url);
	my $res = $ua->simple_request($req);
	my $l = $res->header('Location');
	$seen ||= [];

	return nuke_url $id        if $res->code == 404 || $res->code == 410 || defined($l) && $l =~ m{imgur\.com/removed\.png$};

	# XXX possibly validate Location header before trying to fetch it. Could be relative.
	if ($res->is_redirect) { # recurse
		push $seen, $url;
		return 0 if @$seen > 4; # maximum of 4 redirects
		return check_url $id, $l, $seen   if $res->is_redirect;
	}

	return 0 unless $res->is_success;

	1
}

sub poked {
	my ($id, $url);
	do { ($id, $url) = &grab_url; } while (!check_url($id, $url));
	print $url,"\n";
	STDOUT->flush;
}

# do a blocking read to send the process to sleep until a newline wakes it up
chomp and poked($_) while <STDIN>;
