package require Tclx 8.0

namespace eval cabletv {

variable ns [namespace current]
variable pipe
variable pipew ;# write end of the entertained pipe
variable pid
variable waiting {}
variable attempts 0
variable mydir [file dirname [info script]] ;# directory the script is located in

# Edit these settings
set db-file		"urlmagic.db"
set error-log		"$mydir/error.log"
set pipecmd 		"$mydir/entertained.pl ${db-file} 2>>${error-log}"
set default-chan	"#imgurians-cabletv"
# end of settings

proc open_pipe {} {
	variable ns
	variable pid
	variable pipe
	variable pipew
	variable pipecmd
	lassign [pipe] pr pipew
	set pipe [open "|$pipecmd <@ $pr" r]
	fconfigure $pipe -blocking 0 -buffering line
	fileevent $pipe readable ${ns}::read_pipe
	set pid [pid $pipe]
}

proc close_pipe {} {
	variable pid
	variable pipe
	variable pipew
	variable waiting
	catch {close $pipe}
	catch {close $pipew}
	catch {kill $pid}
	catch {wait $pid} ;# reap zombie
	set waiting {}
	set pipew ""
	set pipe ""
	set pid ""
}

proc reopen_pipe {} {
	close_pipe
	open_pipe
}

proc read_pipe {args} {
	variable ns
	variable pipe
	variable waiting
	variable default-chan
	variable attempts

	set err eof
	if {[catch {gets $pipe line} err] || [eof $pipe]} {
		close_pipe
		if {[incr attempts] > 3} {
			putlog "failed to respawn entertained, giving up. Fix the issue, then invoke .tcl ${ns}::open_pipe manually"
			set attempts 0
		} else {
			putlog "entertained has died, respawning"
			open_pipe
		}
		return
	}
	set attempts 0
	set waiting [lassign $waiting target]
	if {$target eq ""} {
		set target ${default-chan}
	}
	puthelp "PRIVMSG $target :[string trim $line]"
	return
}

proc poke_pipe {chan {msg ""}} {
	variable ns
	variable pipew
	variable waiting
	lappend waiting $chan
	# FIXME code duplication here and in read_pipe
	if {$pipew == "" || [catch {
		puts -nonewline $pipew "$msg\n"
		flush $pipew
	}]} then {
		putlog "Error: trying to poke entertained but it's gone. Try .tcl ${ns}::reopen_pipe"
		return
	}
}

if {[catch {set pid}]} { open_pipe }

proc cron:cabletv {args} {
	variable default-chan
	poke_pipe ${default-chan}
	return
}

proc pub:gif {nick uhost hand chan text} {
        poke_pipe $chan
        return
}

proc pub:any {nick uhost hand chan text} {
	if {[string index $text 0] ne "?"} return
	poke_pipe $chan any
	return
}

bind cron - "*/10 * * * *" ${ns}::cron:cabletv
bind pub - !gif ${ns}::pub:gif
bind pubm - * ${ns}::pub:any

} ;# end namespace
